
package me.saukin.assignment_1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

/**
 *
 * @author saukin
 * 
 * this class reads  txt-file with specified formatting and creates an xml-
 * file based on the information from txt-file 
 * 
 */
public class Assignment_1 {

    /**
     * 
     * this method does prepare a String value to be a valid tag in xml-file
     * 
     * @param s - String value to be prepared
     * @return  String value of tag name for xml-file
     */
    
    public String getTag(String s) {
        String tag;
        tag = s.trim().replaceAll(":", "").replaceAll(" ", "_").replaceAll("[()]", "").replaceAll("\"", "").toLowerCase();
        return tag;
    }
    
    /**
     * 
     * this method does prepare a String value to be a valid tag in xml-file
     * 
     * @param s - String value to be prepared
     * @return  String value of tag name for xml-file
     */
    
    public String getSubTag(String s) {
        String subTag;
        if (s.equals("starring")) {
            subTag = "star";
        } else {
            //removes "s" from "producerS", "writerS" etc
            subTag = s.substring(0, (s.length())-1);
        }
        return subTag;
    }
    
    /**
     * 
     * This method trims and converts String value from scanner (or any other String) to 
     * an array of String values divided by a comma
     * 
     * @param s - String value 
     * @return  array of String values
     */
    
    public String[] trimAndSplit(String s) {
        String [] values = s.trim().split(",");
        return values;
    }
    
    /**
     * This method checks if it's a tag with multiple values or not
     * in case of Movies.txt these are 2nd,6th,7th,8th and 9th tags
     * 
     * It works inside of for-loop cycle to form an element "movie"
     * 
     * @param a - integer value of counter
     * @return  boolean value 
     */
    
    public boolean compareTagIndex(int a) {
        boolean y = false;
        Integer [] subTagIndex = {2, 6, 7, 8, 9};
        for (int i = 0; i < subTagIndex.length; i++) {
            if (subTagIndex[i] == a) y = true;
        }
        return y;
    }
    
    /**
     * 
     * This method read a txt-file by lines and converts these lines into tags, 
     * subtags and descriptions. Then creates xml file based on txt.
     * 
     * 
     * @throws FileNotFoundException
     * @throws ParserConfigurationException
     * @throws TransformerConfigurationException
     * @throws TransformerException 
     */
    
    public void perform() throws FileNotFoundException, 
            ParserConfigurationException,
            TransformerConfigurationException,
            TransformerException  {
    
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        
        // future base of xml document
        Document doc = db.newDocument();                    

        //root element        
        Element moviesRoot = doc.createElement("movies");   
        doc.appendChild(moviesRoot);

        //folder element        
        Element movie;                                      
        //tag element
        Element tagName;                                    
        //subTag element
        Element subTagName;                                 
        
        try (Scanner sc = new Scanner(new File(".\\files\\Movies.txt"))) {
            while (sc.hasNext()) {
                
                // String variable to compare a line from scanner
                String s = sc.nextLine().trim();
                
                // if the line is empty scanner reads next one to get a title
                if (s.equals("")) {
                    movie = doc.createElement("movie");
                    movie.setAttribute("title", sc.nextLine());
                
                // if the line is not empty it is a title
                } else {
                    movie = doc.createElement("movie");
                
                // title is an attribute in my version    
                    movie.setAttribute("title", s);
                }
                
                // for-loop cycle to create a movie folder
                // there are 10 loops because it creates a tag and description/subtags in it
                for (int a = 1; a <= 10; a++) {
                    String tag = getTag(sc.nextLine());
                    tagName = doc.createElement(tag);
                // checking if it is a tag with multiple values    
                    if (compareTagIndex(a)) {
                        String subTag = getSubTag(tag);
                        String [] subTagValues = trimAndSplit(sc.nextLine());
                //  a loop for a case of multiple values to create subtags and descriptions in them       
                        for (int i = 0; i < subTagValues.length; i++) {
                            subTagName = doc.createElement(subTag);
                            Text descriptionText = doc.createTextNode(subTagValues[i].trim());
                            subTagName.appendChild(descriptionText);
                            tagName.appendChild(subTagName);
                            movie.appendChild(tagName);
                        }
                // case of single value in a tag
                    } else {
                        Text descriptionText = doc.createTextNode(sc.nextLine());
                        tagName.appendChild(descriptionText);
                        movie.appendChild(tagName);
                    }
                }
                moviesRoot.appendChild(movie);
            }
        }
        
        DOMSource inputDoc = new DOMSource(doc);
        
        StreamResult outputFile = 
            new StreamResult(".//files//movies.xml");
        
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer t = tFactory.newTransformer();
        t.setOutputProperty(OutputKeys.INDENT, "yes");
        t.transform(inputDoc, outputFile);
    } 
    
    public static void main(String[] args) throws TransformerException, 
            TransformerConfigurationException,
            ParserConfigurationException,
            FileNotFoundException {
        
        Assignment_1 doIt = new Assignment_1();
        doIt.perform();
    
    }
    
}
    

